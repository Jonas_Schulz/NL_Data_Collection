
# -*- coding: utf-8 -*- 
""" 
Created on Thu Jul 20 13:18:05 2023 
 
@author: jonas 
""" 
 
 
import numpy as np 
import time 
import re 
import socket 
import serial 
import matplotlib.pyplot as plt
 
def SetupGloveReadings(): 
    'params' 
    UDP_IP = "127.0.0.1" 
    UDP_PORT = 5005 
    'establish socket' 
    sock = socket.socket(socket.AF_INET, # Internet 
                         socket.SOCK_DGRAM) # UDP 
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 1) 
    # sock.settimeout(0.05) 
    sock.bind((UDP_IP, UDP_PORT)) 
    # print(sock.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF)) 
    return sock 
 
def ReadGloveData(sock):          
    'receive glove data and timestamp' 
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes 
    data_row = re.findall(r"[-+]?\d*\.\d+|\d+", str(data)) 
    return data_row, time.time() 
 
def has_numbers(inputString): 
    return any(char.isdigit() for char in inputString) 
 
#helper function for buffer
def InsertEntryInFILOBuffer(buffer, entry): 
    buffer = np.roll(buffer, -1, axis=0) 
    buffer[buffer.shape[0]-1,:] = entry 
    return buffer     
 
 #return serial port and buffer
def SetupUpFSRReadings(port_name):     
    'Serial Port' 
    port = port_name 
    ser = serial.Serial(port, 9600, timeout=1) 
    dist_buffer = np.zeros((200,1))    
    return ser, dist_buffer 
 
#returns sensor reading and time 
def ReadFSR(ser):     
    data = ser.readline() 
    data = data.decode("utf-8") 
    try:  
        dist = str(data) 
        print(dist)
        if has_numbers(dist): 
            dist = re.findall(r'\d+', data) 
            dist = float(dist[0]) 
            return dist
    except ValueError: return np.nan
     
#returns True if object has been lifted
def CheckIfObjectLifted(dist_buffer):    
    if not np.count_nonzero(dist_buffer): return True
    else: return False
    
def CheckIfAllObjectsLifted(dist_buffer_lst):
    lifted_lst = []
    for element in dist_buffer_lst:
        if not np.count_nonzero(element): lifted_lst.append(True)
        else: lifted_lst.append(False)
     
    return all(lifted_lst)
     
def CollectDataTrial(lift_limit):
    glove_lst = []
    glove_time_lst = []

    fsr_lst = []
    fsr_time_lst = []

    lift_counter = 0

    lifted = False
    start_trial = False#are conditions sufficient to start the trial

    dist_buffer1 = np.zeros((200, 1))

    sock = SetupGloveReadings()
    ser, dist_buffer = SetupUpFSRReadings('COM7')

    loop_time_lst = []

    while lift_counter < lift_limit:
        
        loop_start = time.time()
        while CheckIfAllObjectsLifted([dist_buffer1]):
            fsr_val = ReadFSR(ser)
            dist_buffer1 = InsertEntryInFILOBuffer(dist_buffer1, np.array(fsr_val))
            print('still lifted')
            start_trial = False
        else: 
            lifted = False
            start_trial = True

        
        # if lift_counter < 10:
        print('lift_counter')
        print(lift_counter)
        # data, time_stamp = ReadGloveData(sock)
        # glove_lst.append(data)
        # glove_time_lst.append(time_stamp)
      
        fsr_val = ReadFSR(ser)
        fsr_lst.append(fsr_val)
        fsr_time_lst.append(time.time())
        dist_buffer1 = InsertEntryInFILOBuffer(dist_buffer1, np.array(fsr_val))
        
        if CheckIfObjectLifted(dist_buffer1) and lifted == False: 
            print('lifted')
            lift_counter = lift_counter + 1
            lifted = True
                
        loop_time_lst.append(time.time()-loop_start)
    
    return glove_lst, glove_time_lst, fsr_lst, fsr_time_lst
    
    
